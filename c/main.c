#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include "sort/simple.h"
#include "helper.h"
#include "sort/merge.h"


struct argumentStruct{
	long * list;
	unsigned long size;
};

int main(){
	printf("Reading inputfile\n");
	FILE *file;
	unsigned long size;
	file = fopen("in.bin","rb");
	if(file != NULL){
		fseek(file, 0, SEEK_END);
		size = ftell(file) / sizeof(long);
		fseek(file, 0, SEEK_SET);
	}else{
		size = 50000;
	}

	long *master;
	master = (long *) malloc(size * sizeof(long));
	if(master == NULL){
		printf("Could not allocate memory for master-list!\n");
		return 1;
	}

	if(file == NULL){
		for(unsigned long i = 0; i < size; i++){
			master[i] = size - i;
		}
	}else{
		for(unsigned long i = 0; i < size; i++){
			long tmp;
			fread(&tmp, sizeof(long), 1, file);
			master[i] = tmp;
		}
	}

	printf("Starting sort on %lu elements\n", size);

	long *list;
	list = (long *) malloc(size * sizeof(long));
	if(list == NULL){
		printf("Could not allocate memory for working-list!\n");
		free(master);
		return 1;
	}
	CopyArray(master, 0, size, list);
	
	if(size <= 150000){
		InsertionSort(list, size);
		CopyArray(master, 0, size, list);
	}

	if(size <= 250000){
		SelectionSort(list, size);
		CopyArray(master, 0, size, list);
	}

	int mreturn = MergeSort(list, size);
	if(mreturn == 1){
		printf("Merge-sort not executed, could not allocate memory!\n");
	}

	CopyArray(master, 0, size, list);
	mreturn = MergeSortThread(list, size);
	if(mreturn == 1){
		printf("Merge-sort (threaded) not executed, could not allocate memory!\n");
	}

	free(list);
	free(master);
	return mreturn;
}

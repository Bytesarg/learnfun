#include "simple.h"
#include <time.h>
#include <stdio.h>

void InsertionSort(long * list, unsigned long size){
	struct timespec start, end;
	clock_gettime(CLOCK_REALTIME, &start);

	printf("Starting: insertion-sort\n");

	for(unsigned long pos = 0; pos < size; pos++){
		unsigned long ins = pos;
		while((list[ins - 1] > list[ins]) && (ins > 0)){
			long temp = list[ins - 1];
			list[ins - 1] = list[ins];
			list[ins] = temp;
			ins--;
		}
	}

	clock_gettime(CLOCK_REALTIME, &end);
	unsigned long durationNS = end.tv_nsec - start.tv_nsec;
	unsigned long durationS = end.tv_sec - start.tv_sec;

	printf("Insertion-sort took %lu.%09lu seconds\n", durationS, durationNS);
}

void SelectionSort(long * list, unsigned long size){
	struct timespec start, end;
	clock_gettime(CLOCK_REALTIME, &start);

	printf("Starting: selection-sort\n");

	for(unsigned long pos = 0; pos < size; pos++){
		long smalest;
		unsigned long location;
		smalest = list[pos];
		location = pos;
		for(unsigned long i = pos; i < size; i++){
			if(list[i] < smalest){
				smalest = list[i];
				location = i;
			}
		}
		list[location] = list[pos];
		list[pos] = smalest;
	}

	clock_gettime(CLOCK_REALTIME, &end);
	unsigned long durationNS = end.tv_nsec - start.tv_nsec;
	unsigned long durationS = end.tv_sec - start.tv_sec;

	printf("Selection-sort took %lu.%09lu seconds\n", durationS, durationNS);
}

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <pthread.h>
#include "merge.h"
#include "../helper.h"

#define THREADS 256

struct MergeSortData{
	long * listA;
	long * listB;
	unsigned long begin;
	unsigned long end;
	unsigned long threading;
};

void MergeSortMerge(long * A, unsigned long begin, unsigned long middle, unsigned long end, long * B){
	unsigned long i = begin;
	unsigned long j = middle;

	for(unsigned long k = begin; k < end; k++){
		if(i < middle && (j >= end || A[i] <= A[j])){
			B[k] = A[i];
			i++;
		}else{
			B[k] = A[j];
			j++;
		}
	}
}

void MergeSortSplit(long * A, unsigned long begin, unsigned long end, long * B){
	if(end - begin < 2){
		return;
	}
	unsigned long middle = (end + begin) / 2;

	MergeSortSplit(B, begin, middle, A);
	MergeSortSplit(B, middle, end, A);

	MergeSortMerge(B, begin, middle, end, A);
}

void *MergeSortSplitThread(void * arguments){
	struct MergeSortData *args = arguments;
	unsigned long middle = (args->end + args->begin) / 2;

	if (args->end - args->begin <= args->threading){
		MergeSortSplit(args->listB, args->begin, middle, args->listA);
		MergeSortSplit(args->listB, middle, args->end, args->listA);
	}else{
		pthread_t subA;
		pthread_t subB;
		struct MergeSortData subArgsA;
		struct MergeSortData subArgsB;
		subArgsA.listA = args->listB;
		subArgsB.listA = args->listB;
		subArgsA.listB = args->listA;
		subArgsB.listB = args->listA;
		subArgsA.begin = args->begin;
		subArgsB.begin = middle;
		subArgsA.end = middle;
		subArgsB.end = args->end;
		subArgsA.threading = args->threading;
		subArgsB.threading = args->threading;
		pthread_create(&subA, NULL,&MergeSortSplitThread,(void *)&subArgsA);
		pthread_create(&subB, NULL,&MergeSortSplitThread,(void *)&subArgsB);
		pthread_join(subA, NULL);
		pthread_join(subB, NULL);
	}

	MergeSortMerge(args->listB, args->begin, middle, args->end, args->listA);
}

int MergeSort(long * A, unsigned long size){
	struct timespec start, end;
	clock_gettime(CLOCK_REALTIME, &start);

	long *B;

	B = (long *) malloc(size * sizeof(long));
	if(B == NULL){
		return 1;
	}

	printf("Starting: merge-sort\n");

	CopyArray(A,0,size,B);
	MergeSortSplit(A,0,size,B);
	free(B);

	clock_gettime(CLOCK_REALTIME, &end);
	unsigned long durationNS = end.tv_nsec - start.tv_nsec;
	unsigned long durationS = end.tv_sec - start.tv_sec;
	
	printf("Merge-sort took %lu.%09lu seconds\n", durationS, durationNS);
	
	return 0;
}

int MergeSortThread(long * A, unsigned long size){
	struct timespec start, end;
	clock_gettime(CLOCK_REALTIME, &start);

	long *B;
	B = (long *) malloc(size * sizeof(long));
	if(B == NULL){
		return 1;
	}
	
	printf("Starting: merge-sort (with %d threads)\n", THREADS);

	CopyArray(A,0,size,B);

	pthread_t sub;
	struct MergeSortData subArgs;
	subArgs.listA = A;
	subArgs.listB = B;
	subArgs.begin = 0;
	subArgs.end = size;
	subArgs.threading = size / THREADS;
	pthread_create(&sub, NULL, &MergeSortSplitThread, (void *)&subArgs);
	pthread_join(sub, NULL);
	free(B);

	clock_gettime(CLOCK_REALTIME, &end);
	unsigned long durationNS = end.tv_nsec - start.tv_nsec;
	unsigned long durationS = end.tv_sec - start.tv_sec;
	
	printf("Merge-sort (threaded) took %lu.%09lu seconds\n", durationS, durationNS);
	
	return 0;
}

void MergeSortMerge(long * A, unsigned long begin, unsigned long middle, unsigned long end, long * B);
void MergeSortSplit(long * A, unsigned long begin, unsigned long end, long * B);
void *MergeSortSplitThread(void * arguments);
int MergeSort(long * A, unsigned long size);
int MergeSortThread(long * A, unsigned long size);

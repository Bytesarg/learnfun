/**
 * Description: Copy one array into another
 * Arguments: 	long * A: pointer to source-array
 * 		long * B: pointer to destination-array
 * 		unsigned long start: First array-position to copy
 * 		unsigned long end: Last array-position to copy
 *
 * Note: The destination-array needs to be allocated (ie. by malloc)
 */
void CopyArray(long * A, unsigned long start, unsigned long end, long * B){
	for(unsigned long i = start; i < end; i++){
		// Copy each element to destination array in same position
		B[i] = A[i];
	}
}

Learnfun is intended to teach myself how to code stuff in various languages.
Currently I am focused on relearning C.

The binary takes a file (in.bin) and applies all operations on the data in this file.
Currently the file is read in binary mode and converted to a long-array for sorting purposes.

Insertion-sort is disabled when over 150k elements are loaded.
Selection-sort is disabled when over 250k elements are loaded.

When generating input-data:
*  dd if=/dev/urandom of=in.bin bs=8 count=150000 (for all sorting algorithms)
*  dd if=/dev/urandom of=in.bin bs=8 count=250000 (to exclude insertion-sort)
*  anything with count above 250000 disables both.

Currently multithreaded merge-sort uses hardcoded 256 threads
#!/usr/bin/perl

use NetAddr::IP;
use Net::IP;
use IO::Socket;
use Socket;

my $localip = NetAddr::IP->new(`ip ad sh | grep inet | grep -v docker | grep -v inet6 | grep global | awk '{print \$2}' | head -n 1`);
my $ip = new Net::IP(($localip->network->addr."/".$localip->masklen),4) or die (Net::IP::Error());

while($ip++){
	if($ip->ip() ne $ip->last_ip()){
		my $target = $ip->ip();
		my $result = `ping -c 1 $target | grep 100% | awk '{print \$6}'`;
		chomp $result;
		if(!$result){
			print "Host is up: ".$ip->ip()."\n";
			for(my $port = 1; $port < 65536; $port++){
				my $socket = IO::Socket::INET->new(PeerHost=>$ip->ip(),PeerPort=>$port,Timeout=>0.2,Proto=>'tcp');

				if($socket){
					print "        Port open: $port\n";
				}
			}
		}
	}
}
